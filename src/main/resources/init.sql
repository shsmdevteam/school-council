-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.15-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for smg
CREATE DATABASE IF NOT EXISTS `smg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smg`;

-- Dumping structure for table smg.activities
CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `alternate` int(11) NOT NULL,
  `name` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `datetime` mediumtext NOT NULL,
  `archive` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table smg.activities: ~3 rows (approximately)
DELETE FROM `activities`;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` (`id`, `organizer`, `alternate`, `name`, `description`, `datetime`, `archive`) VALUES
	(1, 1, 2, 'Литературно четене', 'Очаквайте скоро :D', '?? Октомври 2018 г.', b'0'),
	(2, 1, 2, 'Организиране на клубовете в СМГ', 'Събиране на:<br><ol><li>доброволци за водене на клубове</li><li>желаещи да участват в тези клубове</li></ul>', 'До края на октомври', b'0'),
	(3, 1, 2, 'Конкурс за ново лого на СМГ', 'Очаквайте скоро :D', 'До декември', b'0'),
	(4, 1, 2, 'Конкурс за нов дизайн на сайта на СМГ', 'Очаквайте скоро :D', 'До декември', b'0'),
	(5, 1, 2, 'Спортен празник', 'Очаквайте скоро :D', 'Вторият срок :D', b'0'),
   (6, 1, 2, 'Ден на отворените умове', 'Очаквайте скоро :D <br><br>От 9:00 до 18:00 ч.', '26 Април 2019', b'0'),
   (7, 1, 2, 'Концерт на СМГ', 'Очаквайте скоро :D', '19:00 ч., 26 Април 2019', b'0')
    ;
	/*!40000 ALTER TABLE `activities` ENABLE KEYS */;

-- Dumping structure for table smg.clubs
CREATE TABLE IF NOT EXISTS `clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `alternate` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `datetime` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smg.clubs: ~0 rows (approximately)
DELETE FROM `clubs`;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;

-- Dumping structure for table smg.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `telephone` text NOT NULL,
  `custom` text NOT NULL,
  `password` text NOT NULL,
  `role` text NOT NULL DEFAULT 'Student',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smg.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `telephone`, `custom`, `password`, `role`) VALUES
	(1, 'Lili Panaiotova', '+123456789', '<a href="mailto:lili@panaitova.com">lili@panaitova.com</a>', 'nqmam', 'Student'),
	(2, 'Alex Tsvetanov', '+123456789', '<a href="mailto:alex@tsalex.tk">alex@tsalex.tk</a>', 'nqmam', 'Student');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
